import argparse
import math
import cv2

import tensorflow as tf
import numpy as np

from collections import namedtuple

Label = namedtuple('Label', ['name', 'color'])
Size = namedtuple('Size', ['w', 'h'])
Point = namedtuple('Point', ['x', 'y'])
Sample = namedtuple('Sample', ['filename', 'boxes', 'imgsize'])
Box = namedtuple('Box', ['label', 'labelid', 'center', 'size'])
Score = namedtuple('Score', ['idx', 'score'])
Overlap = namedtuple('Overlap', ['best', 'good'])


def str2bool(v):
    """
    Convert a string to a boolean
    """
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def load_data_source(data_source):
    """
    Load a data source given it's name
    """
    source_module = __import__('source_' + data_source)
    get_source = getattr(source_module, 'get_source')
    return get_source()


def rgb2bgr(tpl):
    """
    Convert RGB color tuple to BGR
    """
    res = (tpl[2], tpl[1], tpl[0])
    return res


def abs2prop(xmin, xmax, ymin, ymax, imgsize):
    """
    Convert the absolute min-max box bound to proportional center-width bound
    """
    width = float(xmax - xmin)
    height = float(ymax - ymin)
    cx = float(xmin) + width / 2
    cy = float(ymin) + height / 2
    width /= imgsize.w
    height /= imgsize.h
    cx /= imgsize.w
    cy /= imgsize.h
    return Point(cx, cy), Size(width, height)


def prop2abs(center, size, imgsize):
    """
    Convert proportional center-width bounds to absolute min-max bounds
    """
    width2 = size.w * imgsize.w / 2
    height2 = size.h * imgsize.h / 2
    cx = center.x * imgsize.w
    cy = center.y * imgsize.h
    return int(cx - width2), int(cx + width2), int(cy - height2), int(cy + height2)


def draw_box(img, box, color):
    img_size = Size(img.shape[1], img.shape[0])
    xmin, xmax, ymin, ymax = prop2abs(box.center, box.size, img_size)
    img_box = np.copy(img)
    cv2.rectangle(img_box, (xmin, ymin), (xmax, ymax), color, 2)
    cv2.rectangle(img_box, (xmin - 1, ymin), (xmax + 1, ymin - 20), color, cv2.FILLED)
    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(img_box, box.label, (xmin + 5, ymin - 5), font, 0.5, (255, 255, 255), 1, cv2.LINE_AA)
    alpha = 0.8
    cv2.addWeighted(img_box, alpha, img, 1. - alpha, 0, img)


def initialize_uninitialized_variables(sess):
    """
    Only initialize the weights that have not yet been initialized by other means, such as importing a metegraph and a
    check point. It's useful when extending an existing model.
    """
    uninit_vars = []
    uninit_tensors = []
    for var in tf.global_variables():
        uninit_vars.append(var)
        uninit_tensors.append(tf.is_variable_initialized(var))
    uninit_bools = sess.run(uninit_tensors)
    uninit = zip(uninit_bools, uninit_vars)
    uninit = [var for init, var in uninit if not init]
    sess.run(tf.variables_initializer(uninit))


def box_is_valid(box):
    for x in [box.center.x, box.center.y, box.size.w, box.size.h]:
        if math.isnan(x) or math.isinf(x):
            return False
    return True


def normalize_box(box):
    if not box_is_valid(box):
        return box

    img_size = Size(1000, 1000)
    xmin, xmax, ymin, ymax = prop2abs(box.center, box.size, img_size)
    xmin = max(xmin, 0)
    xmax = min(xmax, img_size.w - 1)
    ymin = max(ymin, 0)
    ymax = min(ymax, img_size.h - 1)

    # this happens early in the training when box min and max are outsize of the images
    xmin = min(xmin, xmax)
    ymin = min(ymin, ymax)

    center, size = abs2prop(xmin, xmax, ymin, img_size)
    return Box(box.label, box.labelid, center, size)


class PrecisionSummary:
    def __init__(self, session, writer, sample_name, labels, restore=False):
        self.session = session
        self.writer = writer
        self.labels = labels

        sess = session
        ph_name = sample_name + '_mAP_ph'
        sum_name = sample_name + '_mAP'

        if restore:
            self.mAP_placeholder = sess.graph.get_tensor_by_name(ph_name + ':0')
            self.mAP_summary_op = sess.graph.get_tensor_by_name(sum_name + ':0')
        else:
            self.mAP_placeholder = tf.placeholder(tf.float32, name=ph_name)
            self.mAP_summary_op = tf.summary.scalar(sum_name, self.mAP_placeholder)

        self.placeholders = {}
        self.summary_ops = {}

        for label in labels:
            sum_name = sample_name + '_AP_' + label
            ph_name = sample_name + '_AP_ph_' + label
            if restore:
                placeholder = sess.graph.get_tensor_by_name(ph_name + ':0')
                summary_op = sess.graph.get_tensor_by_name(sum_name + ':0')
            else:
                placeholder = tf.placeholder(tf.float32, name=ph_name)
                summary_op = tf.summary.scalar(sum_name, placeholder)
            self.placeholders[label] = placeholder
            self.summary_ops[label] = summary_op


class ImageSummary:
    def __init__(self, session, writer, sample_name, colors, restore=False):
        self.session = session
        self.writer = writer
        self.colors = colors

        sess = session
        sum_name = sample_name + '_img'
        ph_name = sample_name + '_img_ph'
        if restore:
            self.img_placeholder = sess.graph.get_tensor_by_name(ph_name + ':0')
            self.img_summary_op = sess.graph.get_tensor_by_name(sum_name + ':0')
        else:
            self.img_placeholder = tf.placeholder(tf.float32, name=ph_name, shape=[None, None, None, 3])
            self.img_summary_op = tf.summary.image(sum_name, self.img_placeholder)


class LossSummary:
    def __init__(self, session, writer, sample_name, num_samples, restore=False):
        self.session = session
        self.writer = writer
        self.num_samples = num_samples
        self.loss_names = ['total', 'localization', 'confidence', 'l2']
        self.loss_values = {}
        self.placeholders = {}

        sess = session

        summary_ops = []
        for loss in self.loss_names:
            sum_name = sample_name + '_' + loss + '_loss'
            ph_name = sample_name + '_' + loss + '_loss_ph'

            if restore:
                placeholder = sess.graph.get_tensor_by_name(ph_name + ':0')
                summary_op = sess.graph.get_tensor_by_name(sum_name + ':0')
            else:
                placeholder = tf.placeholder(tf.float32, name=ph_name)
                summary_op = tf.summary.scalar(sum_name, placeholder)

            self.loss_values[loss] = float(0)
            self.placeholders[loss] = placeholder
            summary_ops.append(summary_op)

        self.summary_ops = tf.summary.merge(summary_ops)

    def add(self, values, num_samples):
        for loss in self.loss_names:
            self.loss_values[loss] += values[loss] * num_samples
