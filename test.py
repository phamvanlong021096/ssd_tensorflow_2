import tensorflow as tf

# negatives_num_max = tf.range(0, 10, 1)
# negatives_num_max_t = tf.expand_dims(negatives_num_max, 1)
# rng = tf.range(0, 5, 1)
# range_row = tf.expand_dims(rng, 0)
#
# negatives_max_mask = tf.less(range_row, negatives_num_max_t)
#
# with tf.Session() as sess:
#     res1, res2, res3 = sess.run([range_row, negatives_num_max_t, negatives_max_mask])
#     print(res1)
#     print('*' * 50)
#     print(res2)
#     print('*' * 50)
#     print(res3)

a = tf.constant([[1, 2, 3, 4, 5],
                 [1, 2, 3, 4, 5],
                 [1, 2, 3, 4, 5],
                 [1, 2, 3, 4, 5],
                 [1, 2, 3, 4, 5],
                 [1, 2, 3, 4, 5],
                 [1, 2, 3, 4, 5],
                 [1, 2, 3, 4, 5],
                 [1, 2, 3, 4, 5],
                 [1, 2, 3, 4, 5]])
b = tf.nn.top_k(a, 5)

with tf.Session() as sess:
    res = sess.run(b)
    print(res)
