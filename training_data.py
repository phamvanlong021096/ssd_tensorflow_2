import pickle
import random
import math
import cv2
import os

import multiprocessing as mp
import numpy as np
import queue as q

from copy import copy


class TrainingData:
    def __init__(self, data_dir):
        try:
            with open(data_dir + '/training-data.pkl', 'rb') as f:
                data = pickle.load(f)
            with open(data_dir + '/train-samples.pkl', 'rb') as f:
                train_samples = pickle.load(f)
            with open(data_dir + '/valid-samples.pkl', 'rb') as f:
                valid_samples = pickle.load(f)
        except (FileNotFoundError, IOError) as e:
            raise RuntimeError(str(e))

        nones = [None] * len(train_samples)
        train_samples = list(zip(nones, nones, train_samples))
        nones = [None] * len(valid_samples)
        valid_samples = list(zip(nones, nones, valid_samples))

        # Set the attributes up
        self.preset = data['preset']
        self.num_classes = data['num-classes']
        self.label_colors = data['colors']
        self.lid2name = data['lid2name']
        self.lname2id = data['lname2id']
        self.train_tfs = data['train-transforms']
        self.valid_tfs = data['valid-transforms']
        self.train_generator = self.__batch_generator(train_samples, self.train_tfs)
        self.valid_generator = self.__batch_generator(valid_samples, self.valid_tfs)
        self.num_train = len(train_samples)
        self.num_valid = len(valid_samples)
        self.train_samples = list(map(lambda x: x[2], train_samples))
        self.valid_samples = list(map(lambda x: x[2], valid_samples))

    def __batch_generator(self, sample_list_, transforms):
        image_size = (self.preset.image_size.w, self.preset.image_size.h)

        def run_transforms(sample):
            args = sample
            for t in transforms:
                args = t(*args)
            return args

        def process_samples(samples):
            images = []
            labels = []
            gt_boxes = []
            for s in samples:
                done = False
                counter = 0
                while not done and counter < 50:
                    image, label, gt = run_transforms(s)
                    num_bg = np.count_nonzero(label[:, self.num_classes])
                    done = num_bg < label.shape[0]
                    counter += 1

                images.append(image.astype(np.float32))
                labels.append(label.astype(np.float32))
                gt_boxes.append(gt.boxes)

            images = np.array(images, dtype=np.float32)
            labels = np.array(labels, dtype=np.float32)
            return images, labels, gt_boxes

        def gen_batch(batch_size, num_workers=0):
            sample_list = copy(sample_list_)
            random.shuffle(sample_list)
            if num_workers > 0:
                pass
            else:
                for offset in range(0, len(sample_list), batch_size):
                    samples = sample_list[offset:offset + batch_size]
                    images, labels, gt_boxes = process_samples(samples)
                    yield images, labels, gt_boxes

        return gen_batch
