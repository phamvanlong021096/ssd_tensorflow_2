import numpy as np

from collections import defaultdict
from ssdutils import jaccard_overlap
from utils import Size, prop2abs

IMG_SIZE = Size(1000, 1000)


class APCalculator:
    """
    Compute average precision of object detection as used in PASCAL VOC
    Challenges. It is a peculiar measure because of the way it calculates the
    precision-recall curve. It's highly sensitive to the sorting order of the
    predictions in different images. Ie. the exact same resulting bounding
    boxes in all images may get different AP score depending on the way
    the boxes are sorted globally by confidence.
    Reference: http://homepages.inf.ed.ac.uk/ckiw/postscript/ijcv_voc09.pdf
    Reference: http://host.robots.ox.ac.uk/pascal/VOC/voc2007/VOCdevkit_08-Jun-2007.tar
    """

    def __init__(self, minoverlap=0.5):
        """
        Initialize the calculator.
        """
        self.minoverlap = minoverlap
        self.clear()

    def clear(self):
        """
        Clear the current detection cache. Useful for restarting the calculation for a new batch of data.
        """
        self.det_params = defaultdict(list)
        self.det_confidence = defaultdict(list)
        self.det_sample_ids = defaultdict(list)
        self.gt_boxes = []
